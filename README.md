![logo](./proximity_detection/imgs/logo.png)

# synsense-modelzoo

This repo contains the synsense demos that build based on Speck/DynapCNN product series. It relies on series of the stable version python packages to run. Each demo should have a standard working enviorments which is illustated in their correspond sub-folders.

For any enquiries or help pls contact:

Bo Li: bo.li@synsense.ai\
Yannan Xing: yannan.xing@synsense.ai \
Yalun Hu: yalun.hu@synsense.ai

## Requirements:

* Ubuntu 16.04 or later
* Python3.6 or higher

At your python enviornment run:

``
pip install -r requirements.txt
``

if you physically located in china its faster with the aliyun pypi source

``
pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/
``

``
Up to this moment, you need to additionally install sinabs-dynapcnn, pip install sinabs-dynapcnn --pre``

## Udev rules on Linux systems
You will probably need to add the following udev rules in order for Samna to detect any devices connected to your system.

For SynSense devices, make a file in /etc/udev/rules.d named 60-synsense.rules which contains the following:

```
SUBSYSTEM=="usb", ATTR{idVendor}=="04b4", MODE="0666"
SUBSYSTEM=="usb", ATTR{idVendor}=="152a", MODE="0666"
SUBSYSTEM=="usb", ATTR{idVendor}=="337d", MODE="0666"
```


## For Demo Users:

Quick start:

At root dir of this repository:

``
python run_proximity_detection.py 
``

For the first time of running this script, you may found it is actually installing the samna,
but this only happens for the first run of the entirely new enviornment. If you want to run an older version of the model based on speck2b, please move to the dev branch

### Support demo:

* `smartdoor`

### Support device:

* `Speck2eDevKit`

* `Speck2eTestBoard`

* `Speck2bDevKit`

* `Speck2bDevKitTiny`

* `DynapcnnDevKit`

  



### Demo Video:

`It should be reminded that speck2e uses a 3.6mm lens and is placed at a height between 80-110cm; elevation angle between 15-22°. (Suggested test height 110cm)`



![Visualizer](./proximity_detection/imgs/smartDoor.gif)





