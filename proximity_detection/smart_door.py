class StateMachineSmartDoor(object):
    def __init__(self,
                 digital_labels,
                 SILENT_THRESH,
                 readout_time_milli_second,
                 ):

        self.prediction = "silent"
        self.readout_time_milli_second = readout_time_milli_second
        self.SILENT_THRESH = SILENT_THRESH
        self.digital_labels = digital_labels
        self.control_list = None
        self.flag = True
        self.last_place_holder = (-1, time.time())
        self.current_place_holder = (-1, time.time())
        cv2.namedWindow('tmp', 0)

        silent_img = cv2.imread('./readout_icons/silent.png')
        near1m_img = cv2.imread('./readout_icons/Near1m.png')
        near2m_img = cv2.imread('./readout_icons/Near2m.png')
        near3m_img = cv2.imread('./readout_icons/Near3m.png')
        # away1m_img = cv2.imread('./readout_icons/Near1m.png')
        away2m_img = cv2.imread('./readout_icons/Away2m.png')
        away3m_img = cv2.imread('./readout_icons/Away3m.png')
        left_img = cv2.imread('./readout_icons/left.png')
        right_img = cv2.imread('./readout_icons/right.png')
        self.result_imgs = {"near3": near3m_img,
                            "near2": near2m_img,
                            "near1": near1m_img,
                            "away2": away2m_img,
                            "away3": away3m_img,
                            "silent": silent_img,
                            "left": left_img,
                            "right": right_img,
                            }

    def gesture_readout_state_machine(self, buffer, visualizer):

        self.prediction = "silent"

        while visualizer.is_open():
            events = buffer.get_events()
            # if len(events) == 0: print(f"num of spk: {len(events)}")
            results = get_spikes_of_each_class(events, self.digital_labels)
            print(results)
            sorted_results: list = sorted(results.items(), key=lambda x: x[1], reverse=True)
            prediction: int = sorted_results[0][0] if sorted_results[0][1] > self.SILENT_THRESH else -1
            print(f"prediction: {prediction}")

            if self.control_list is None:
                self.control_list = []

            self.control_list.append(prediction) if prediction not in self.control_list else None
            print(self.control_list)
            if len(self.control_list) >= 2:
                if (self.control_list[1] == 3) and self.control_list[0] == -1:
                    self.prediction = "left"
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)
                    tmp = self.control_list[1]
                    self.control_list = []
                    self.control_list.append(tmp)

                elif (self.control_list[1] == 4) and self.control_list[0] == -1:
                    self.prediction = "right"
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)
                    tmp = self.control_list[1]
                    self.control_list = []
                    self.control_list.append(tmp)

                elif (self.control_list[0] == 3 or self.control_list[0] == 4 or self.control_list[0] == 0) and self.control_list[1] == -1:
                    self.prediction = "silent"
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)
                    self.control_list = []
                    self.control_list.append(-1)

                elif 3 in self.control_list and 4 in self.control_list:
                    self.control_list = []

                elif (self.control_list[0] == 4 or self.control_list[0] == 3) and self.control_list[1] == 2:
                    # self.prediction = "silent"
                    # cv2.imshow('tmp', self.result_imgs[self.prediction])
                    self.control_list = []
                    self.control_list.append(2)

                elif 3 in self.control_list or 4 in self.control_list:
                    self.control_list = None

                elif -1 in self.control_list and (3 in self.control_list or 4 in self.control_list):

                    self.prediction = "silent"
                    self.control_list = []


                #### 基本逻辑在这里 接近3米
                elif self.control_list[-1] == 0 and self.control_list[0] == -1:
                    self.prediction = "near3"
                    self.control_list = []
                    self.control_list.append(0)
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)
                ## 接近2米
                elif self.control_list[-1] == 1 and self.control_list[0] == 0:
                    self.prediction = "near2"
                    self.control_list = []
                    self.control_list.append(1)
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)
                ## 接近1米
                elif self.control_list[-1] == 2 and self.control_list[0] == 1:
                    self.prediction = "near1"
                    self.control_list = []
                    self.control_list.append(2)
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)

                elif self.control_list[-1] == 1 and self.control_list[0] == 2:
                    self.prediction = "away2"
                    self.control_list = []
                    self.control_list.append(1)
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)

                elif self.control_list[-1] == 0 and self.control_list[0] == 1:
                    self.prediction = "away3"
                    self.control_list = []
                    self.control_list.append(0)
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)

                # elif self.control_list[0] == 2 and (4 in self.control_list or 3 in self.control_list):
                #     self.control_list = None
            else:
                if -1 in self.control_list:
                    self.prediction = "silent"
                    cv2.imshow('tmp', self.result_imgs[self.prediction])
                    cv2.waitKey(500)
                print("No Define.....")

            cv2.imshow('tmp', self.result_imgs[self.prediction])
            cv2.waitKey(500)
            time.sleep(self.readout_time_milli_second / 1000)